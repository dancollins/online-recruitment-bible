
# [Online Recruitment Bible](http://net-recruit.co.uk/online-recruitment-bible)

Getting to grips with online recruitment can be difficult, especially because articles about the subject can be hard to find.

This page is a collection of the best articles we've found on the subject of online recruitment

* Source: [https://bitbucket.org/dancollins/online-recruitment-bible](https://bitbucket.org/dancollins/online-recruitment-bible)
* Homepage: [http://net-recruit.co.uk](http://net-recruit.co.uk)
* Twitter: [@Net_RecruitUK](https://twitter.com/Net_RecruitUK)